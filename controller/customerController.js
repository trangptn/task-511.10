const customerModel=require("../model/customerModel");
const mogoose=require('mongoose');

const createCustomer= async (req,res) =>{
    //B1: thu thap du lieu
    const{
        reqFullName,
        reqPhone,
        reqEmail,
        reqAddress,
        reqCity,
        reqCountry
    }=req.body
    //B2: validate du lieu
    if(!reqFullName){
        res.status(400).json({
            message:"full name khong hop le"
        })
    }
    if(!reqPhone){
        res.status(400).json({
            message:"Phone khong hop le"
        })
    }
    if(!reqEmail){
        res.status(400).json({
            message:"Email khong hop le"
        })
    }
    try{
        //B3:Xu ly du lieu
        var newCustomer={
            fullname:reqFullName,
            phone:reqPhone,
            email:reqEmail,
            address:reqAddress,
            city:reqCity,
            country:reqCountry
        }

        const result= await customerModel.create(newCustomer);
        return res.status(201).json({
            message:"Tao customer thanh cong",
            data: result
        })
    }
    catch(error){
        return error.status(500).json({
            message:"Co loi xay ra"
        })
    }
}

const getAllCustomer= async (req,res) =>{
    //B1:Thu thap du lieu
    //B2:Validate du lieu
    //B3:Xu ly du lieu
    try {
        const result= await customerModel.find();
        return res.status(200).json({
            message:"Lay danh sach customer thanh cong",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message:"Co loi xay ra"
        })
    }
}

const getCustomerById= async (req,res) =>{
    //B1:Thu thap du lieu
    const customerid=req.params.customerid;

    //B2:Validate du lieu
    if(!mogoose.Types.ObjectId.isValid(customerid)){
        return res.status(400).json({
            message:"Customer Id khong hop le"
        })
    }

    //B3:Xu ly du lieu
    const result= await customerModel.findById(customerid);
    try {
       
        return res.status(200).json({
            message:"Lay danh sach customer thanh cong",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message:"Co loi xay ra"
        })
    }
}

const updateCustomer= async (req,res) =>{
     //B1:Thu thap du lieu
     const customerid=req.params.customerid;
     const{
        reqFullName,
        reqPhone,
        reqEmail,
        reqAddress,
        reqCity,
        reqCountry
    }=req.body
     //B2: validate du lieu
     if(reqFullName===""){
        res.status(400).json({
            message:"Full name khong hop le"
        })
    }
    if(reqEmail===""){
        res.status(400).json({
            message:"Email khong hop le"
        })
    }
    if(reqPhone===""){
        res.status(400).json({
            message:"Phone khong hop le"
        })
    }
    if(reqEmail===""){
        res.status(400).json({
            message:"Email khong hop le"
        })
    }
   

    //B3: Xu ly du lieu
    try {
        var newUpdateCustomer= {};
        if(reqFullName){
            newUpdateCustomer.fullname=reqFullName;
        }
        if(reqPhone){
            newUpdateCustomer.phone=reqPhone;
        }
        if(reqAddress){
            newUpdateCustomer.address=reqAddress;
        }
        if(reqCity){
            newUpdateCustomer.city=reqCity;
        }
        if(reqEmail){
            newUpdateCustomer.email=reqEmail;
        }
        if(reqCountry){
            newUpdateCustomer.country=reqCountry;
        }

        var result =await customerModel.findByIdAndUpdate(customerid, newUpdateCustomer);

        if(result)
        {
            return res.status(200).json({
                message:"Cap nhat du lieu thanh cong",
                data:result
            })
        }
        else{
            return res.status(404).json({
                message:"Khong tim thay thong tin customer",
            })
        }
       
    } catch (error) {
        return res.status(500).json({
            message:"Co loi xay ra"
        })
    }
}

const deleteCustomer=async (req,res) =>{
    //B1:Thu thap du lieu
    const customerid=req.params.customerid;
    //B2:
    if(!mogoose.Types.ObjectId.isValid(customerid)){
        return res.status(400).json({
            message:"Customer Id khong hop le"
        })
    }
    try {
        const result =await customerModel.findByIdAndDelete(customerid);

        if(result)
        {
            return res.status(200).json({
                message:"Xoa du lieu thanh cong",
            })
        }
        else{
            return res.status(404).json({
                message:"Khong tim thay thong tin customer",
            })
        }
    } catch (error) {
        return res.status(500).json({
            message:"Co loi xay ra"
        })
    }
}

module.exports={
    createCustomer,
    getAllCustomer,
    getCustomerById,
    updateCustomer,
    deleteCustomer
}