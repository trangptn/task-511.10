const orderModel=require("../model/orderModel");
const customerModel=require('../model/customerModel');

const mongoose= require('mongoose');

const createOrder= async (req,res) =>{
    //B1: thu thap du lieu
    const{
        customerid,
        reqShippedDate,
        reqNote,
        reqCost
    }=req.body
    //B2: validate du lieu
    if(!mongoose.Types.ObjectId.isValid(customerid)){
        return res.status(400).json({
            message:"Customer Id khong hop le"
        })
    }
    if(!Number.isInteger(reqCost)||reqCost<0){
        res.status(400).json({
            message:"Cost khong hop le"
        })
    }
    try{
        //B3:Xu ly du lieu
        var newOrder={
            shippepDate:reqShippedDate,
            note:reqNote,
            cost:reqCost
        }

        const result= await orderModel.create(newOrder);
        const da=await customerModel.findByIdAndUpdate(customerid,{
            $push:{type:result._id}
        });
        if(da){
        return res.status(201).json({
            message:"Tao order thanh cong",
            data: result
        })
    }
    }
    catch(error){
        return error.status(500).json({
            message:"Co loi xay ra"
        })
    }
}

const getAllOrder= async (req,res) =>{
    //B1:Thu thap du lieu
    //B2:Validate du lieu
    //B3:Xu ly du lieu
    try {
        const result= await orderModel.find();
        return res.status(200).json({
            message:"Lay danh sach order thanh cong",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message:"Co loi xay ra"
        })
    }
}

const getOrderById= async (req,res) =>{
    //B1:Thu thap du lieu
    const orderid=req.params.orderid;

    //B2:Validate du lieu
    if(!mongoose.Types.ObjectId.isValid(orderid)){
        return res.status(400).json({
            message:"Order Id khong hop le"
        })
    }

    //B3:Xu ly du lieu
    const result= await orderModel.findById(orderid);
    try {
       
        return res.status(200).json({
            message:"Lay danh sach order thanh cong",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message:"Co loi xay ra"
        })
    }
}

const updateOrder= async (req,res) =>{
     //B1:Thu thap du lieu
     const orderid=req.params.orderid;
     const{
        reqShippedDate,
        reqNote,
        reqCost
    }=req.body
     //B2: validate du lieu
     if(reqCost<0){
        res.status(400).json({
            message:"Cost khong hop le"
        })
    }

   

    //B3: Xu ly du lieu
    try {
        var newUpdateOrder= {};
        if(reqShippedDate){
            newUpdateOrder.shippepDate=reqShippedDate;
        }
        if(reqNote){
            newUpdateOrder.note=reqNote;
        }
        if(reqCost){
            newUpdateOrder.cost=reqCost;
        }

        var result =await orderModel.findByIdAndUpdate(orderid, newUpdateOrder);

        if(result)
        {
            return res.status(200).json({
                message:"Cap nhat du lieu thanh cong",
                data:result
            })
        }
        else{
            return res.status(404).json({
                message:"Khong tim thay thong tin order",
            })
        }
       
    } catch (error) {
        return res.status(500).json({
            message:"Co loi xay ra"
        })
    }
}

const deleteOrder=async (req,res) =>{
    //B1:Thu thap du lieu
    const orderid=req.params.orderid;

    var customerid=req.query.customerid;
    //B2:
    if(!mongoose.Types.ObjectId.isValid(orderid)){
        return res.status(400).json({
            message:"Order Id khong hop le"
        })
    }
    try {
        const result =await orderModel.findByIdAndDelete(orderid);
        if(customerid !==undefined)
        {
           await customerModel.findByIdAndUpdate(customerid,{
                $pull:{type:orderid}
            })
        }
        if(result)
        {
            return res.status(200).json({
                message:"Xoa du lieu thanh cong",
            })
        }
        else{
            return res.status(404).json({
                message:"Khong tim thay thong tin order",
            })
        }
    } catch (error) {
        return res.status(500).json({
            message:"Co loi xay ra"
        })
    }
}

module.exports={
    createOrder,
    getAllOrder,
    getOrderById,
    updateOrder,
    deleteOrder
}