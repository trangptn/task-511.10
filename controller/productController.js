const productModel=require("../model/productModel");
const mogoose=require('mongoose');

const createProduct= async (req,res) =>{
    //B1: thu thap du lieu
    const{
        reqName,
        reqDescription,
        reqImageUrl,
        reqBuyPrice,
        reqPromotionPrice,
        reqAmount
    }=req.body
    //B2: validate du lieu
    if(!reqName){
        res.status(400).json({
            message:"Product name khong hop le"
        })
    }
    if(!reqImageUrl){
        res.status(400).json({
            message:"Image url khong hop le"
        })
    }
    if(!Number.isInteger(reqPromotionPrice)){
        res.status(400).json({
            message:"Promotion price khong hop le"
        })
    }
    if(!Number.isInteger(reqBuyPrice)|| reqBuyPrice==undefined){
        res.status(400).json({
            message:"Price khong hop le"
        })
    }
    if(!Number.isInteger(reqAmount)||reqAmount<0){
        res.status(400).json({
            message:"Amount khong hop le"
        })
    }
    try{
        //B3:Xu ly du lieu
        var newProduct={
            name:reqName,
            description:reqDescription,
            imageUrl:reqImageUrl,
            buyPrice:reqBuyPrice,
            promotionPrice:reqPromotionPrice,
            amount:reqAmount
        }

        const result= await productModel.create(newProduct);
        return res.status(201).json({
            message:"Tao product thanh cong",
            data: result
        })
    }
    catch(error){
        return error.status(500).json({
            message:"Co loi xay ra"
        })
    }
}

const getAllProduct= async (req,res) =>{
    //B1:Thu thap du lieu
    //B2:Validate du lieu
    //B3:Xu ly du lieu
    try {
        const result= await productModel.find();
        return res.status(200).json({
            message:"Lay danh sach product thanh cong",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message:"Co loi xay ra"
        })
    }
}

const getProductById= async (req,res) =>{
    //B1:Thu thap du lieu
    const productid=req.params.productid;

    //B2:Validate du lieu
    if(!mogoose.Types.ObjectId.isValid(productid)){
        return res.status(400).json({
            message:"Product Id khong hop le"
        })
    }

    //B3:Xu ly du lieu
    const result= await productModel.findById(productid);
    try {
       
        return res.status(200).json({
            message:"Lay danh sach product thanh cong",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message:"Co loi xay ra"
        })
    }
}

const updateProduct= async (req,res) =>{
     //B1:Thu thap du lieu
     const productid=req.params.productid;
     const{
        reqName,
        reqDescription,
        reqImageUrl,
        reqBuyPrice,
        reqPromotionPrice,
        reqAmount
    }=req.body
     //B2: validate du lieu
     if(reqName===""){
        res.status(400).json({
            message:"Product name khong hop le"
        })
    }
    if(reqImageUrl===""){
        res.status(400).json({
            message:"Image url khong hop le"
        })
    }
    if(reqPromotionPrice===""){
        res.status(400).json({
            message:"Promotion price khong hop le"
        })
    }
   

    //B3: Xu ly du lieu
    try {
        var newUpdateProduct= {};
        if(reqName){
            newUpdateProduct.name=reqName;
        }
        if(reqDescription){
            newUpdateProduct.description=reqDescription;
        }
        if(reqImageUrl){
            newUpdateProduct.imageUrl=reqImageUrl;
        }
        if(reqBuyPrice){
            newUpdateProduct.buyPrice=reqBuyPrice;
        }
        if(reqPromotionPrice){
            newUpdateProduct.promotionPrice=reqPromotionPrice;
        }
        if(reqAmount){
            newUpdateProduct.amount=reqAmount;
        }

        var result =await productModel.findByIdAndUpdate(productid, newUpdateProduct);

        if(result)
        {
            return res.status(200).json({
                message:"Cap nhat du lieu thanh cong",
                data:result
            })
        }
        else{
            return res.status(404).json({
                message:"Khong tim thay thong tin product",
            })
        }
       
    } catch (error) {
        return res.status(500).json({
            message:"Co loi xay ra"
        })
    }
}

const deleteProduct=async (req,res) =>{
    //B1:Thu thap du lieu
    const productid=req.params.productid;
    //B2:
    if(!mogoose.Types.ObjectId.isValid(productid)){
        return res.status(400).json({
            message:"Product Id khong hop le"
        })
    }
    try {
        const result =await productModel.findByIdAndDelete(productid);

        if(result)
        {
            return res.status(200).json({
                message:"Xoa du lieu thanh cong",
            })
        }
        else{
            return res.status(404).json({
                message:"Khong tim thay thong tin product",
            })
        }
    } catch (error) {
        return res.status(500).json({
            message:"Co loi xay ra"
        })
    }
}

module.exports={
    createProduct,
    getAllProduct,
    getProductById,
    updateProduct,
    deleteProduct
}