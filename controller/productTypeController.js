const productTypeModel=require('../model/productTypeModel');
const productModel=require('../model/productModel');
const mogoose=require('mongoose');

const createProductType= async (req,res) =>{
    //B1: thu thap du lieu
    const{
        productid,
        productTypeName,
        reqDescription
    }=req.body
    console.log(req.body);
    //B2: validate du lieu
    if(!mogoose.Types.ObjectId.isValid(productid)){
        return res.status(400).json({
            message:"Product Id khong hop le"
        })
    }
    if(!productTypeName){
        res.status(400).json({
            message:"Product type name khong hop le"
        })
    }

    try{
        //B3:Xu ly du lieu
        var newProductType={
            name:productTypeName,
            description:reqDescription
        }

        const result= await productTypeModel.create(newProductType);
        const da=await productModel.findByIdAndUpdate(productid,{
                $push:{type:result._id}
            });
            if(da)
            {
                return res.status(201).json({
                    message:"Tao product type thanh cong",
                    data: result
                })
            }
       
    }
    catch(error){
        console.log(error);
        return error.status(500).json({
            message:"Co loi xay ra"
        })
    }
}

const getAllProductType= async (req,res) =>{
    //B1:Thu thap du lieu
    //B2:Validate du lieu
    //B3:Xu ly du lieu
    try {
        const result= await productTypeModel.find();
        return res.status(200).json({
            message:"Lay danh sach product type thanh cong",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message:"Co loi xay ra"
        })
    }
}

const getProductTypeById= async (req,res) =>{
    //B1:Thu thap du lieu
    const productTypeid=req.params.productTypeid;

    //B2:Validate du lieu
    if(!mogoose.Types.ObjectId.isValid(productTypeid)){
        return res.status(400).json({
            message:"Product type Id khong hop le"
        })
    }

    //B3:Xu ly du lieu
    const result= await productTypeModel.findById(productTypeid);
    try {
       
        return res.status(200).json({
            message:"Lay danh sach product type thanh cong",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message:"Co loi xay ra"
        })
    }
}

const updateProductTypeById= async (req,res) =>{
     //B1:Thu thap du lieu
     const productTypeid=req.params.productTypeid;
     const{
        productTypeName,
        reqDescription
    }=req.body
     //B2: validate du lieu
     if(productTypeName ===""){
        res.status(400).json({
            message:"Product type name khong hop le"
        })
    }

    //B3: Xu ly du lieu
    try {
        var newUpdateProductType= {};
        if(productTypeName){
            newUpdateProductType.name=productTypeName;
        }
        if(reqDescription){
            newUpdateProductType.description=reqDescription;
        }

        var result =await productTypeModel.findByIdAndUpdate(productTypeid, newUpdateProductType);

        if(result)
        {
            return res.status(200).json({
                message:"Cap nhat du lieu thanh cong",
                data:result
            })
        }
        else{
            return res.status(404).json({
                message:"Khong tim thay thong tin product type",
            })
        }
       
    } catch (error) {
        return res.status(500).json({
            message:"Co loi xay ra"
        })
    }
}

const deleteProductType=async (req,res) =>{
    //B1:Thu thap du lieu
    const productTypeid=req.params.productTypeid;

    //Xoa id thua trong mang type them productid
    var productid=req.query.productid;
    //B2:
    if(!mogoose.Types.ObjectId.isValid(productTypeid)){
        return res.status(400).json({
            message:"Product type Id khong hop le"
        })
    }
    try {
        const result =await productTypeModel.findByIdAndDelete(productTypeid);

        if(productid !==undefined)
        {
           await productModel.findByIdAndUpdate(productid,{
                $pull:{type:productTypeid}
            })
        }
        if(result)
        {
            return res.status(200).json({
                message:"Xoa du lieu thanh cong",
            })
        }
        else{
            return res.status(404).json({
                message:"Khong tim thay thong tin product type",
            })
        }
    } catch (error) {
        return res.status(500).json({
            message:"Co loi xay ra"
        })
    }
}

module.exports={
    createProductType,
    getAllProductType,
    getProductTypeById,
    updateProductTypeById,
    deleteProductType
}