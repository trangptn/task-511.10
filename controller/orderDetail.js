const orderDetailModel=require("../model/orderDetail");
const orderModel=require('../model/orderModel');

const mongoose= require('mongoose');

const createOrderDetail= async (req,res) =>{
    //B1: thu thap du lieu
    const{
        reqQuality,
        orderid
    }=req.body
    //B2: validate du lieu
    if(!mongoose.Types.ObjectId.isValid(orderid)){
        return res.status(400).json({
            message:"Order Id khong hop le"
        })
    }

    if(!Number.isInteger(reqQuality)||reqCost<0){
        res.status(400).json({
            message:"Quality khong hop le"
        })
    }
    try{
        //B3:Xu ly du lieu
        var newOrderDetail={
            quality:reqQuality
        }

        const result= await orderDetailModel.create(newOrderDetail);
        const da=await orderModel.findByIdAndUpdate(orderid,{
            $push:{type:result._id}
        });
        if(da){
        return res.status(201).json({
            message:"Tao order detail thanh cong",
            course:da,
            data: result
        })
    }
    }
    catch(error){
        return error.status(500).json({
            message:"Co loi xay ra"
        })
    }
}

const getAllOrderDetail= async (req,res) =>{
    //B1:Thu thap du lieu
    //B2:Validate du lieu
    //B3:Xu ly du lieu
    try {
        const result= await orderDetailModel.find();
        return res.status(200).json({
            message:"Lay danh sach order detail thanh cong",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message:"Co loi xay ra"
        })
    }
}

const getOrderDetailById= async (req,res) =>{
    //B1:Thu thap du lieu
    const orderdetailid=req.params.orderdetailid;

    //B2:Validate du lieu
    if(!mongoose.Types.ObjectId.isValid(orderdetailid)){
        return res.status(400).json({
            message:"Order detail Id khong hop le"
        })
    }

    //B3:Xu ly du lieu
    const result= await orderDetailModel.findById(orderdetailid);
    try {
       
        return res.status(200).json({
            message:"Lay danh sach order detail thanh cong",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message:"Co loi xay ra"
        })
    }
}

const updateOrderDetail= async (req,res) =>{
     //B1:Thu thap du lieu
     const orderdetailid=req.params.orderdetailid;
     const{
        reqQuality
    }=req.body
     //B2: validate du lieu
     if(reqQuality<0){
        res.status(400).json({
            message:"Quality khong hop le"
        })
    }

   

    //B3: Xu ly du lieu
    try {
        var newUpdateOrderDetail= {};
        if(reqQuality){
            newUpdateOrderDetail.quality=reqQuality;
        }

        var result =await orderDetailModel.findByIdAndUpdate(orderdetailid, newUpdateOrderDetail);

        if(result)
        {
            return res.status(200).json({
                message:"Cap nhat du lieu thanh cong",
                data:result
            })
        }
        else{
            return res.status(404).json({
                message:"Khong tim thay thong tin order detail",
            })
        }
       
    } catch (error) {
        return res.status(500).json({
            message:"Co loi xay ra"
        })
    }
}

const deleteOrderDetail=async (req,res) =>{
    //B1:Thu thap du lieu
    const orderdetailid=req.params.orderdetailid;

    var orderid=req.query.orderid;
    //B2:
    if(!mongoose.Types.ObjectId.isValid(orderdetailid)){
        return res.status(400).json({
            message:"Order detail Id khong hop le"
        })
    }
    try {
        const result =await orderDetailModel.findByIdAndDelete(orderdetailid);
        if(orderid !==undefined)
        {
           await orderModel.findByIdAndUpdate(orderid,{
                $pull:{type:orderdetailid}
            })
        }
        if(result)
        {
            return res.status(200).json({
                message:"Xoa du lieu thanh cong",
            })
        }
        else{
            return res.status(404).json({
                message:"Khong tim thay thong tin order detail",
            })
        }
    } catch (error) {
        return res.status(500).json({
            message:"Co loi xay ra"
        })
    }
}

module.exports={
    createOrderDetail,
    getAllOrderDetail,
    getOrderDetailById,
    updateOrderDetail,
    deleteOrderDetail
}