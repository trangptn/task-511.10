const mongoose=require('mongoose');

const schema = mongoose.Schema;

const orderDetailSchema= new schema({
    product:[
        {
        type:mongoose.Types.ObjectId,
        ref:"product"
        }
    ],
    quantity:{
        type:Number,
        default:0
    }
})