const mongoose=require('mongoose');

const schema= mongoose.Schema;

const orderSchelma= new schema({
    orderDate:{
        type:Date,
        default:Date.now()
    },
    shippepDate:{
        type:Date
    },
    note:{
        type:String
    },
    orderDetail:{
        type:mongoose.Types.ObjectId,
        ref:"orderDetail"
    },
    cost:{
        type:Number,
        default:0
    }
})
module.exports=mongoose.model("order",orderSchelma);