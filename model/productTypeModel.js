const { default: mongoose } = require('mongoose');
const mogoose=require('mongoose');

const schema=mogoose.Schema;

const  productTypeSchema= new schema({
    name:{
        type: String,
        unique: true,
        require: true
    },
    description:{
        type: String,
        require: false
    },
    },
    {
        timestamps: true
    })

module.exports= mongoose.model("productType",productTypeSchema);

