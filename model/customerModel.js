const mongoose=require('mongoose');

const schema= mongoose.Schema;

const customerSchema=new schema({
    fullname:{
        type:String,
        require:true
    },
    phone:{
        type:String,
        require:true,
        unique:true
    },
    email:{
        type:String,
        require:true,
        unique:true
    },
    address:{
        type:String,
        default:""
    },
    city:{
        type:String,
        default:""
    },
    country:{
        type:String,
        default:""
    },
    orders:[
        {
        type:mongoose.Types.ObjectId,
        ref:"order"
        }
    ]
},
{
    timestamps:true
})

module.exports=mongoose.model("customer",customerSchema);