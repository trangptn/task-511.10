const mongoose=require('mongoose');

const schema=mongoose.Schema;

const productSchema= new schema({
    name:{
        type:String,
        unique:true,
        require: true
    },
    description:{
        type:String,
        require:false
    },
    type:[
        {
            type:mongoose.Types.ObjectId,
            ref:"productType"
        }
    ],
    imageUrl:{
        type:String,
        require:true
    },
    buyPrice:{
        type:Number,
        require:true
    },
    promotionPrice:{
        type:Number,
        require:true
    },
    amount:{
        type:Number,
        default:0
    }
},
{
    timestamps:true
}
)

module.exports= mongoose.model("product",productSchema);