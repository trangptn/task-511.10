const express=require('express');

const router=express.Router();

const{
    createOrderDetail,
    getAllOrderDetail,
    getOrderDetailById,
    updateOrderDetail,
    deleteOrderDetail
}=require("../controller/orderDetail");

router.post("/",createOrderDetail);

router.get("/",getAllOrderDetail);

router.get("/:orderdetailid",getOrderDetailById);

router.put("/:orderdetailid",updateOrderDetail);

router.delete("/:orderdetailid",deleteOrderDetail);

module.exports=router;