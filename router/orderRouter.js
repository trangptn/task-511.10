const express=require('express');

const router=express.Router();

const{
    createOrder,
    getAllOrder,
    getOrderById,
    updateOrder,
    deleteOrder
}=require("../controller/orderController");

router.post("/",createOrder);

router.get("/",getAllOrder);

router.get("/:orderid",getOrderById);

router.put("/:orderid",updateOrder);

router.delete("/:orderid",deleteOrder);

module.exports=router;