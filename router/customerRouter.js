const express=require('express');

const router=express.Router();

const{
    createCustomer,
    getAllCustomer,
    getCustomerById,
    updateCustomer,
    deleteCustomer
}=require("../controller/customerController");

router.post("/",createCustomer);

router.get("/",getAllCustomer);

router.get("/:customerid",getCustomerById);

router.put("/:customerid",updateCustomer);

router.delete("/:customerid",deleteCustomer);

module.exports=router;