const express=require('express');

const router= express.Router();

const {
    createProductType,
    getAllProductType,
    getProductTypeById,
    updateProductTypeById,
    deleteProductType
} = require("../controller/productTypeController");

router.post("/",createProductType);

router.get("/",getAllProductType);

router.get("/:productTypeid",getProductTypeById);

router.put("/:productTypeid",updateProductTypeById);

router.delete("/:productTypeid",deleteProductType);

module.exports=router;
