const express=require('express');

const router=express.Router();

const{
    createProduct,
    getAllProduct,
    getProductById,
    updateProduct,
    deleteProduct
}=require("../controller/productController");

router.post("/",createProduct);

router.get("/",getAllProduct);

router.get("/:productid",getProductById);

router.put("/:productid",updateProduct);

router.delete("/:productid",deleteProduct);

module.exports=router;