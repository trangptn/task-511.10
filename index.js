//Import thu vien express
const express=require('express');

const mongoose=require('mongoose');

const productTypeRouter=require("./router/productTypeRouter");
const productRouter=require("./router/productRouter");
const customerRouter=require("./router/customerRouter");
const orderRouter=require("./router/orderRouter");
const orderDetailRouter=require("./router/orderDetailRouter");
//Khoi tao app express
const app= new express();

//Khai bao cong
const port=8000;

//Cau hinh su dung json
app.use(express.json());

//Khai bao ket noi MongoDb qua mogoose
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Shop24h")
.then((data)=>{
    console.log("Connect mongoDB Successfully");
})
.catch((err)=>{
    console.log(err);
})


//Su dung router
app.use("/api/v1/productTypes", productTypeRouter);
app.use("/api/v1/products",productRouter);
app.use("/api/v1/customers",customerRouter);
app.use("/api/v1/orders",orderRouter);
app.use("/api/v1/orderdetails",orderDetailRouter);
//Start up
app.listen(port,()=>{
    console.log(`app listening on port ${port}`);
})